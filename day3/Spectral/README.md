# NOVA_NMBU_Spectral_Imaging_Training

This note book is a part of the course "Computer vision for plant phenotyping" delievered by Wageningen University & Research. The notebook and the data is developed by Puneet Mishra and updated by Gerrit Polder. The note book has two exercises as mentioned in the presentaiton of the course. The first exercise is about understanding spectral images and the second is a case of drought stress detection in arabidopsis plant with unsupervised data modelling.


## Getting started

1. go to : https://colab.research.google.com/
2. Login with any google account or make a google account
3. Download the data and scripts from here to your local machine
4. Upload the notebook and data to colab (note uploading the mat file takes some time)
5. Go step by step through the notebook
6. There are two assignements in which you need to provide some code:



